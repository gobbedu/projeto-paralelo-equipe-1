# Projeto Paralelo Equipe 1

## O que é?
O Projeto Paralelo é desenvolver em equipe um dos layouts que iremos disponibilizar.
Cabe à equipe decidir qual layout desenvolver.

Clique aqui para acessar os 3 layouts.
OU
Acesse a pasta:
Ecomp > Diretorias > Projetos > [Trainee] Projeto Paralelo  > Trainees - Projeto Paralelo 2021/2

Dentro dela vocês encontrarão 3 pastas, que são os layouts. Dentro de cada uma dessas pastas tem um arquivo PNG com a página completa e uma pasta de imagens.

Como vocês vão fazer esse layout é tarefa de vocês!!!
Façam horários juntos, trabalhem juntos e se organizem.

## Como?
É obrigatório o uso de HTML, CSS e Bootstrap. 
É obrigatório , também, o uso do Git. 

Além da fidelidade ao layout vai ser cobrado Responsividade. Total responsividade, ou seja, funcionar em celular e tablet de maneira adequada.

E, claro, utilizem o Bootstrap. Leiam a documentação. Na maioria dos casos só esse framework já vai resolver o problema de vocês.

## Entrega?
A entrega do Projeto Paralelo é obrigatória.

Data de entrega: até 02/11/2021 às 23:59:59

Instruções para a entrega:

A partir do momento que o projeto for criado no gitlab.com (sim, deve ser utilizado esse servidor) vocês DEVEM me adicionar no projeto. Meu usuário é @TTeuZ, me adicionem com permissão total (master/maintainer).

Quando o projeto estiver terminado, mandem um email para projetos@ecomp.co informando a entrega, com o link para o projeto no gitlab.com.

